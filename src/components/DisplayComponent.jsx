import React, { Component } from 'react';
import TableComponents from './TableComponent';

class DisplayComponent extends Component {
    handleUser(name) {
        this.props.deleteUser(name)
    }
    render() {
        return (
            <div>
            <table style={{width:500}}>
                <thead>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                </thead>
                <tbody>
                {this.props.personalInfo.map((user, index) =>{
                    return <TableComponents userInfo={user} key={index}
                    deleteUser={this.handleUser.bind(this)}
                    />
                })}
                </tbody>
                </table>
            </div>
        );
    }
}

export default DisplayComponent;
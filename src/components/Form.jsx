import React, { Component } from 'react';
import DisplayComponent from "./DisplayComponent"

class Form extends Component {

    
    state = {
        id:1,
        your_name:"",
        phone_no:"",
        email: "",
        address: "",
        personalInformation:[],
        validationMessageForName:""
    
    }
    handleChange(e){
        let{name} = e.target;
        let {value} = e.target;
        if(name=== "your_name"){
            this.setState({
                your_name:value
            })    
    } else if (name=== "phone_no"){
        this.setState({
            phone_no:value
        })
    } else if (name === "email"){
        this.setState({
            email:value
        })
    }else if(name=== "address"){
        this.setState({
            address:value
        })
    }
}
handleSubmit(){
       
    let {personalInformation, your_name, phone_no, email, address} = this.state;
    if(your_name ===""){
       this.setState({
           validationMessageForName:"Name is required"
       })
    } else{
        personalInformation.push({
            id: this.state.id +1,
            full_name: your_name,
            phoneNumber: phone_no,
            emailAddress:email,
            yourAddress: address
        })
        
        this.setState({
            personalInformation,
            your_name: "",
            phone_no: "",
            email:"",
            address:""
        })
    }

}
handleDelete = (your_name) => {
    const personalInformation= this.state.personalInformation.filter(user => user.full_name !== your_name)
    this.setState({personalInformation:personalInformation})
}

showData() {
    return <DisplayComponent 
    personalInfo={this.state.personalInformation} 
        deleteUser={this.handleDelete.bind(this)}
    />
}

    render() {
        return (
            <div>
                <h1>Controlled Type Form</h1>
                <div id ='form'>
                    <div id = 'name'>
                        <label>Name:</label>
                        <input type="text"
                            name="your_name"
                            value={this.state.your_name}
                            onChange={(e) => this.handleChange(e)}
                        />
                        <p>{this.state.validationMessageForName}</p>
                    </div>
                    <div id = 'phone_no'>
                        <label>Phone No:</label>
                        <input type="number"
                            name="phone_no"
                            value={this.state.phone_no}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div id = 'email'>
                        <label>Email:</label>
                        <input type="text"
                            name="email"
                            value={this.state.email}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div id = 'address'>
                        <label>Address:</label>
                        <input type="text"
                            name="address"
                            value={this.state.address}
                            onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div>
                        <button  onClick={() =>this.handleSubmit()}>Submit</button>
                    </div>
                </div>
                <h1>Table Form</h1>
                {this.showData()}
            </div>
        );
    }
}

export default Form;
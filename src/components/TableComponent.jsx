import React, { Component } from 'react';

class TableComponent extends Component {
    deleteRow(name) {
        this.props.deleteUser(name)
    }
    render() {
        return (
            
                <tr>
                    <td>{this.props.userInfo.full_name}</td>
                    <td>{this.props.userInfo.phoneNumber}</td>
                    <td>{this.props.userInfo.emailAddress}</td>
                    <td>{this.props.userInfo.yourAddress}</td>
                    <td><button style={{height:30}} onClick = {() =>this.deleteRow(this.props.userInfo.full_name)}>Delete</button></td>
                </tr>
            
        );
    }
}

export default TableComponent;